﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.UI;

public class GameEngine : MonoBehaviour
{
    public static bool isTest = true;

    // json file and object type to load.
    public static Dictionary<string, Type> fridayJsonFiles = new Dictionary<string, Type>() {
        { "robinson_cards", typeof(RobinsonCard) },
        { "aging_cards", typeof(AgingCard) },
        { "hazard_cards", typeof(HazardCard) }
    };
    // json file and object type to load.
    public static Dictionary<string, Type> fridayJsonFiles_test = new Dictionary<string, Type>() {
        { "robinson_cards_test", typeof(RobinsonCard) },
        { "aging_cards_test", typeof(AgingCard) },
        { "hazard_cards_test", typeof(HazardCard) }
    };
    public HazardDeck hazardDeck;
    public RobinsonDeck robinsonDeck;
    public AgingDeck agingDeck;

    List<Card> allCards = new List<Card>();
    List<RobinsonCard> allRobinsons = new List<RobinsonCard>();
    List<AgingCard> allAging = new List<AgingCard>();
    List<HazardCard> allHazard = new List<HazardCard>();

    // Use this for initialization
    void Start()
    {
        try
        {
            Dictionary<string, Type> jsonSelected = new Dictionary<string, Type>();
            if (isTest)
            {
                jsonSelected = fridayJsonFiles_test;
            }
            else
            {
                jsonSelected = fridayJsonFiles;
            }

            // Deserialization of all objects from json.
            foreach (string s in jsonSelected.Keys)
            {
                Type t;
                if (jsonSelected.TryGetValue(s, out t))
                {
                    TextAsset objTxt = Resources.Load("json_cards/" + s) as TextAsset;
                    JSONNode jsonString = JSON.Parse(objTxt.text);
                    List<object> objs = new List<object>();
                    LoadFromJSon(t, jsonString, ref objs);

                    // lazy
                    switch (s)
                    {
                        case "robinson_cards_test":
                        case "robinson_cards":
                            allRobinsons = objs.Cast<RobinsonCard>().ToList();
                            foreach (Card c in allRobinsons) { c.defaultPath = "front_cards/rc/"; }
                            break;

                        case "aging_cards_test":
                        case "aging_cards":
                            allAging = objs.Cast<AgingCard>().ToList();
                            foreach (Card c in allAging) { c.defaultPath = "front_cards/ac/"; }
                            break;

                        case "hazard_cards_test":
                        case "hazard_cards":
                            allHazard = objs.Cast<HazardCard>().ToList();
                            foreach (Card c in allHazard) { c.defaultPath = "front_cards/hc/"; }
                            break;
                    }
                }
            }

            // Get the robinson cards for each hazard card.
            List<HazardCard> tmpHC = new List<HazardCard>();
            foreach (HazardCard hc in allHazard)
            {
                // foreach del array de las robinson
                foreach (string rcID in hc.robinsonCards)
                {
                    HazardCard tHC;
                    RobinsonCard tRC;

                    // Make a copy of the new hazardCard and RobinsonCard
                    tHC = new HazardCard(hc);
                    tRC = new RobinsonCard(allRobinsons.Where(r => r.cardID == rcID).First());
                    hc.robinsonCard = new RobinsonCard(allRobinsons.Where(r => r.cardID == rcID).First());

                    tHC.robinsonCard = tRC;
                    allCards.Add(tHC);
                    tmpHC.Add(tHC);

                    //Debug.Log("Created HC: "+ tHC.title +" with RC: "+ tRC.title);
                }
            }
            allHazard.Clear();
            allHazard.AddRange(tmpHC);
            tmpHC.Clear();

            // foreach robinson "normal" card.
            allCards.AddRange(allRobinsons.Where(
                                                 r => r.cardID == "RC1" ||
                                                 r.cardID == "RC2" ||
                                                 r.cardID == "RC3" ||
                                                 r.cardID == "RC4" ||
                                                 r.cardID == "RC"
                                ).Cast<Card>().ToList());
            // get aging cards.
            allCards.AddRange(allAging.Cast<Card>().ToList());

            agingDeck.SetUp(allAging);
            robinsonDeck.SetUp(allRobinsons, null); // TODO: robinsonDeck.SetUp(allRobinsons, (AgingCard)agingDeck.RetrieveRandomCard());
            hazardDeck.SetUp(allHazard);
            Debug.Log("INFO - GameEngine - Start - finished loading object from json.");

            // TODO remove this
            Game.currentGame.ClearCurrentGame();
            Game.currentGame.NewGame(1);
        }
        catch (Exception e)
        {
            Debug.LogError(string.Format("ERROR - GameEngine - Start - Error while deserializing cards: " + e.Message, e.StackTrace));
            allCards = null;
        }
    }

    /// <summary>
    /// Load all objects founded in the jNode content to the casted type typetoload, then, if the object is correct it gets loaded into whereToLoadObjects list.
    /// </summary>
    /// <param name="typeToLoad"></param>
    /// <param name="jNode"></param>
    /// <param name="whereToLoadObjects"></param>
    public void LoadFromJSon(Type typeToLoad, JSONNode jNode, ref List<object> whereToLoadObjects)
    {
        object result = null;

        try
        {
            JSONArray arrayData = jNode["cards"].AsArray;
            // ...load every object...
            //Debug.Log(string.Format("INFO - LoadFromJSon - trying to read the json node."));
            foreach (JSONClass jObject in arrayData)
            {
                result = CastJsonsToObject(jObject, typeToLoad);
                if (result != null)
                {
                    whereToLoadObjects.Add(result);
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError(string.Format("ERROR - LoadFromJSon - Error while deserializing json objects: " + e.Message, e.StackTrace));
        }
    }

    /// <summary>
    /// Deserialize a list of cards.
    /// </summary>
    /// <param name="typeToCast"></param>
    /// <returns></returns>
    public object CastJsonsToObject(JSONClass jsonStringOfObject, Type typeToCast)
    {
        object mySerializedBeautifulObject = null;
        try
        {
            mySerializedBeautifulObject = JsonUtility.FromJson(jsonStringOfObject.ToString(), typeToCast);
        }
        catch (Exception ex)
        {
            Debug.LogError(string.Format("ERROR - CastJsonsToObject - Error while deserializing cards from json: " + ex.Message, ex.StackTrace));
            mySerializedBeautifulObject = null;
        }

        return mySerializedBeautifulObject;
    }
}