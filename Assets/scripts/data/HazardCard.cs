﻿using System;
using UnityEngine;

[Serializable]
public class HazardCard : Card
{
    public string title;
    // beating value by difficulty.
    public int greenHazardValue;
    public int yellowHazardValue;
    public int redHazardValue;
    // free cards to draw.
    public int freeCardsValue;
    // robinson card object.
    public RobinsonCard robinsonCard;

    // array of robinson ID cards to spawn.
    [HideInInspector]
    public string[] robinsonCards;

    public HazardCard() { }

    /// <summary>
    /// Note that this object doesn't have the robinson card list.
    /// </summary>
    /// <param name="cardToCopy"></param>
    public HazardCard(HazardCard cardToCopy)
    {
        this.cardID = cardToCopy.cardID;
        this.robinsonCards = cardToCopy.robinsonCards;
        this.uniqueID = cardToCopy.uniqueID;
        this.defaultPath = cardToCopy.defaultPath;
        this.title = cardToCopy.title;
        this.greenHazardValue = cardToCopy.greenHazardValue;
        this.yellowHazardValue = cardToCopy.yellowHazardValue;
        this.redHazardValue = cardToCopy.redHazardValue;
        this.freeCardsValue = cardToCopy.freeCardsValue;
        this.robinsonCard = cardToCopy.robinsonCard;
    }
}