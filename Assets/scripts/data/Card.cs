﻿using System;

[Serializable]
public class Card
{
    public string cardID;
    public string uniqueID;
    public string defaultPath = string.Empty;
}