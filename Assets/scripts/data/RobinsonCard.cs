﻿using System;

[Serializable]
public class RobinsonCard : Card
{
    // This values are for aging cards too.
    public int lifeCost;
    public int fightingValue;
    public string title;
    public string specialAbilityDescription;

    // Check the booleans and use a delegate to run the special ability.
    // Special ability's for aging cards.
    public bool hasStopAbility;
    public bool hasHighestCardAbility;

    public bool hasDestroyAbility;
    public bool hasBelowThePileAbility;

    public bool hasPhaseAbility;
    public bool hasCopyAbility;

    public bool hasSortCardsAbility;
    public bool hasDoubleAbility;

    public bool hasExchangeAbility;
    public bool hasDoubleExchangeAbility;

    // with amount, first is for aging card too.
    public bool hasChangeLifeAbility;
    public int changeLifeAbilityAmount;

    public bool hasDrawCardsAbility;
    public int drawCardsAbilityAmount;

    public RobinsonCard() { }

    /// <summary>
    /// Copycat constructor.
    /// </summary>
    /// <param name="cardToCopy"></param>
    public RobinsonCard(RobinsonCard cardToCopy)
    {
        this.cardID = cardToCopy.cardID;
        this.uniqueID = cardToCopy.uniqueID;
        this.lifeCost = cardToCopy.lifeCost;
        this.fightingValue = cardToCopy.fightingValue;
        this.title = cardToCopy.title;
        this.specialAbilityDescription = cardToCopy.specialAbilityDescription;
        this.defaultPath = cardToCopy.defaultPath;

        //Abilitys.
        this.hasStopAbility = cardToCopy.hasStopAbility;
        this.hasHighestCardAbility = cardToCopy.hasHighestCardAbility;
        this.hasDestroyAbility = cardToCopy.hasDestroyAbility;
        this.hasBelowThePileAbility = cardToCopy.hasBelowThePileAbility;
        this.hasPhaseAbility = cardToCopy.hasPhaseAbility;
        this.hasCopyAbility = cardToCopy.hasCopyAbility;
        this.hasSortCardsAbility = cardToCopy.hasSortCardsAbility;
        this.hasDoubleAbility = cardToCopy.hasDoubleAbility;
        this.hasExchangeAbility = cardToCopy.hasExchangeAbility;
        this.hasDoubleExchangeAbility = cardToCopy.hasDoubleExchangeAbility;

        // Amount abilitys.
        this.hasChangeLifeAbility = cardToCopy.hasChangeLifeAbility;
        this.changeLifeAbilityAmount = cardToCopy.changeLifeAbilityAmount;
        this.hasDrawCardsAbility = cardToCopy.hasDrawCardsAbility;
        this.drawCardsAbilityAmount = cardToCopy.drawCardsAbilityAmount;
    }
}