using System;

[Serializable]
public class AgingCard : RobinsonCard
{
    // Aging cards is not really used, it's just a robinson card with this.
    public bool isDifficultType;

    public AgingCard() { }
    public AgingCard(AgingCard cardToCopy)
    {
        this.cardID = cardToCopy.cardID;
        this.isDifficultType = cardToCopy.isDifficultType;
        this.uniqueID = cardToCopy.uniqueID;

        this.lifeCost = cardToCopy.lifeCost;
        this.fightingValue = cardToCopy.fightingValue;
        this.title = cardToCopy.title;
        this.specialAbilityDescription = cardToCopy.specialAbilityDescription;
        this.defaultPath = cardToCopy.defaultPath;

        //Abilitys.
        this.hasStopAbility = cardToCopy.hasStopAbility;
        this.hasHighestCardAbility = cardToCopy.hasHighestCardAbility;
        this.hasDestroyAbility = cardToCopy.hasDestroyAbility;
        this.hasBelowThePileAbility = cardToCopy.hasBelowThePileAbility;
        this.hasPhaseAbility = cardToCopy.hasPhaseAbility;
        this.hasCopyAbility = cardToCopy.hasCopyAbility;
        this.hasSortCardsAbility = cardToCopy.hasSortCardsAbility;
        this.hasDoubleAbility = cardToCopy.hasDoubleAbility;
        this.hasExchangeAbility = cardToCopy.hasExchangeAbility;
        this.hasDoubleExchangeAbility = cardToCopy.hasDoubleExchangeAbility;

        // Amount abilitys.
        this.hasChangeLifeAbility = cardToCopy.hasChangeLifeAbility;
        this.changeLifeAbilityAmount = cardToCopy.changeLifeAbilityAmount;
        this.hasDrawCardsAbility = cardToCopy.hasDrawCardsAbility;
        this.drawCardsAbilityAmount = cardToCopy.drawCardsAbilityAmount;
    }
}