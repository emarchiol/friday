﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum StateOfGame : int
{
    CHOOSE_CARD = 0,
    SPECIAL_ABILITY = 1,
    PAY_SELECTED_CARDS = 2,
    NEXT_HAZARD = 3
};
public enum HazardPhase : int
{
    GREEN = 1,
    YELLOW = 2,
    RED = 3,
    PIRATE_1 = 4,
    PIRATE_2 = 5
};
public class Game : MonoBehaviour
{
    // Game instance values.
    private int difficult;
    public int lives;
    private int livesReserve;
    private int livesToPay = -1;
    private int freeCardsToDraw;

    //Fighting values.
    public int hazardFightingValue = -1;
    public int robinsonFightingValue;
    public bool canDrawRobCards = true;
    public int differenceToPayValue = -1;

    // Texts...
    public Text txtLifeCounter;
    public Text payCardsButtonText;

    // Buttons
    public Button payHazardButton;

    // Info texts...
    public Text hazardFightingText;
    public Text robinsonFightingText;
    public Text freeCardsLeftText;
    public Text hazardValueText;

    // Decks...
    private HazardDeck hazardDeck;
    private RobinsonDeck robinsonDeck;
    private AgingDeck agingDeck;

    // Game Objects
    public List<RobinsonCardScript> cardsToPayFor;
    public GameEngine ge;

    private static Game instance;
    public HazardPhase currentAgeDifficult;

    private Game() { }

    /// <summary>
    /// Sort of a singleton constructor. TODO: check this.
    /// </summary>
    public static Game currentGame
    {
        get
        {
            if (instance == null)
            {
                // Assets load.
                //return instance ? null : (instance = (new GameObject("GameEngine")).AddComponent<Game>());
                instance = GameObject.Find("GameEngine").AddComponent<Game>();

                // Info Texts.
                instance.txtLifeCounter = GameObject.Find("text_counter").GetComponent<Text>();
                instance.hazardFightingText = GameObject.Find("hazardFightingText").GetComponent<Text>();
                instance.robinsonFightingText = GameObject.Find("robinsonFightingText").GetComponent<Text>();
                instance.hazardValueText = GameObject.Find("hazardValueText").GetComponent<Text>();
                instance.freeCardsLeftText = GameObject.Find("freeCardsLeftText").GetComponent<Text>();
                instance.payCardsButtonText = GameObject.Find("button_pay_cards").GetComponent<Button>().GetComponentInChildren<Text>();

                //Game Objects.
                instance.cardsToPayFor = new List<RobinsonCardScript>();
                instance.ge = GameObject.Find("GameEngine").GetComponent<GameEngine>();

                // Buttons
                instance.payHazardButton = GameObject.Find("button_pay_cards").GetComponent<Button>();

                // decks
                instance.hazardDeck = GameObject.Find("hazard_deck").GetComponent<HazardDeck>();
                instance.robinsonDeck = GameObject.Find("robinson_deck").GetComponent<RobinsonDeck>();
                instance.agingDeck = GameObject.Find("aging_deck").GetComponent<AgingDeck>();
            }

            return instance;
        }
    }

    public void Update()
    {
    }

    /// <summary>
    /// Modify the GUI button to a different state.
    /// </summary>
    /// <param name="state"></param>
    public void SetPayHazardButtonListener(StateOfGame state)
    {
        switch (state)
        {
            case StateOfGame.CHOOSE_CARD:
                this.payCardsButtonText.text = "Choose cards";
                this.payHazardButton.onClick.RemoveAllListeners();
                this.payHazardButton.onClick.AddListener(() => SelectCardsToPay());
                break;

            case StateOfGame.PAY_SELECTED_CARDS:
                this.payCardsButtonText.text = "Pay for the rest";
                this.payHazardButton.onClick.RemoveAllListeners();
                this.payHazardButton.onClick.AddListener(() => PayForTheRest());
                break;

            case StateOfGame.NEXT_HAZARD:
                this.payCardsButtonText.text = "Next Hazard..";
                this.payHazardButton.onClick.RemoveAllListeners();
                this.payHazardButton.onClick.AddListener(() => NewHazard());
                break;
                /*
                case StateOfGame.SPECIAL_ABILITY:
                    this.payHazardButton.onClick.RemoveAllListeners();
                    this.payHazardButton.onClick.AddListener(() => UseAbility());
                    break;
                */
        }
    }

    /// <summary>
    /// Change the difficult to the next phase and restart the hazard deck.
    /// </summary>
    public void NextDifficultPhase()
    {
        switch (currentAgeDifficult)
        {
            case HazardPhase.GREEN:
                currentAgeDifficult = HazardPhase.YELLOW;
                hazardDeck.RestartDeck();
                hazardValueText.text = "Level: yellow";
                break;

            case HazardPhase.YELLOW:
                currentAgeDifficult = HazardPhase.RED;
                hazardValueText.text = "Level: red";
                hazardDeck.RestartDeck();
                break;

            case HazardPhase.RED:
                currentAgeDifficult = HazardPhase.PIRATE_1;
                hazardValueText.text = "Level: pirate 1";
                hazardDeck.RestartDeck();
                break;

            case HazardPhase.PIRATE_1:
                currentAgeDifficult = HazardPhase.PIRATE_2;
                hazardValueText.text = "Level: pirate 2";
                hazardDeck.RestartDeck();
                break;

            case HazardPhase.PIRATE_2:
                // You did it.
                break;
        }
    }

    /// <summary>
    /// returns the amount of free cards available.
    /// </summary>
    /// <returns></returns>
    public int FreeCardsToDraw()
    {
        freeCardsLeftText.text = "free cards: " + freeCardsToDraw.ToString();
        return freeCardsToDraw;
    }

    public void ClearFreeCards()
    {
        freeCardsToDraw = 0;
        freeCardsLeftText.text = "free cards: " + freeCardsToDraw.ToString();
    }

    /// <summary>
    /// Modify the free cards by the amount - o +.
    /// </summary>
    /// <param name="amount"></param>
    /// <returns></returns>
    public int ChangeFreeCardsToDraw(int amount)
    {
        freeCardsToDraw += amount;
        freeCardsLeftText.text = "free cards: " + freeCardsToDraw.ToString();
        return freeCardsToDraw;
    }

    /// <summary>
    /// Use negative value to sustract.
    /// </summary>
    /// <param name="amount"></param>
    public void ChangeRobinsonFightingValue(int amount)
    {
        robinsonFightingValue += amount;
        if (robinsonFightingValue >= hazardFightingValue)
        {
            canDrawRobCards = false;
            SetPayHazardButtonListener(StateOfGame.NEXT_HAZARD);
        }
        robinsonFightingText.text = "rob fight: " + robinsonFightingValue.ToString();
    }

    /// <summary>
    /// Clean the current phase and draw 2 new hazard cards.
    /// </summary>
    public void NewHazard()
    {
        foreach (Transform gcard in this.robinsonDeck.spawnPanel)
        {
            robinsonDeck.AddCardToDiscarded(gcard.gameObject.GetComponent<RobinsonCardScript>());
        }

        foreach (Transform gcard in this.robinsonDeck.spawnPayedCardsPanel)
        {
            robinsonDeck.AddCardToDiscarded(gcard.gameObject.GetComponent<RobinsonCardScript>());
        }

        robinsonDeck.freeRobinsonCards.Clear();
        robinsonDeck.payedRobinsonCards.Clear();

        bool hasWon = false;
        if (robinsonFightingValue >= hazardFightingValue)
        {
            hasWon = true;
        }

        if (hasWon)
        {
            // Won the hazard, get that robinson.
            HazardCardScript hcsToDestroy = hazardDeck.hazardFightingCard;
            robinsonDeck.AddCardToDiscarded(hcsToDestroy._card);

            // Destroy the hazard ! yeeah
            Destroy(hcsToDestroy.GetComponent<Image>());
            Destroy(hcsToDestroy.GetComponent<LayoutElement>());
            Destroy(hcsToDestroy.gameObject);
        }
        else
        {
            // Lost, hazard back to discarded hazard pile.
            hazardDeck.DiscardCard(hazardDeck.hazardFightingCard);
        }

        if (hazardDeck.cards.Count == 0)
        {
            hazardDeck.RestartDeck();
        }

        hazardDeck.cardsSpawned = false;
        robinsonFightingValue = 0;
        hazardFightingValue = 0;
        freeCardsToDraw = 0;

        SetPayHazardButtonListener(StateOfGame.CHOOSE_CARD);
    }

    /// <summary>
    /// Select cards to pay for.
    /// </summary>
    public void SelectCardsToPay()
    {
        //hace:
        // Cambia el boton de las free cards y las payed cards por uno que las "selecciona" para saber cuales quiere eliminar
        foreach (RobinsonCardScript fRcs in robinsonDeck.freeRobinsonCards)
        {
            fRcs.SetOnClickAction(StateOfGame.CHOOSE_CARD);
        }

        foreach (RobinsonCardScript pRcs in robinsonDeck.payedRobinsonCards)
        {
            pRcs.SetOnClickAction(StateOfGame.CHOOSE_CARD);
        }
        // cambia este botón
        SetPayHazardButtonListener(StateOfGame.PAY_SELECTED_CARDS);
    }

    /// <summary>
    /// The actual "transaction"...
    /// </summary>
    public void PayForTheRest()
    {
        // una vez pagado las cartas seleccionadas se eliminan, las restantes van al robinson discard y la hazard va al discard
    }

    public void SetHazardFightingValue(int amount)
    {
        hazardFightingValue = amount;
        hazardFightingText.text = "haz fight: " + hazardFightingValue.ToString();
    }

    /// <summary>
    /// Reset variables to start a new game.
    /// </summary>
    public void ClearCurrentGame()
    {
        difficult = 0;
        lives = 0;
        livesReserve = 0;
        freeCardsToDraw = 0;
        // means no card is present.
        hazardFightingValue = -1;
        robinsonFightingValue = 0;
    }

    /// <summary>
    /// Generate a new game from difficulty 1.
    /// </summary>
    /// <param name="difficultLevel"></param>
    public void NewGame(int difficultLevel)
    {
        freeCardsToDraw = 0;
        hazardFightingValue = -1;
        robinsonFightingValue = 0;
        difficult = difficultLevel;
        currentAgeDifficult = HazardPhase.GREEN;
        hazardValueText.text = "Level: green";

        switch (difficultLevel)
        {
            case 1:
                lives = 20;
                livesReserve = 2;
                txtLifeCounter.text = "Lives:" + lives.ToString() + "/" + livesReserve.ToString();
                // Remove very stupid aging card.
                agingDeck.RemoveSpecificAgingCard("AC4"); // very stupid card.
                break;

            case 2:
                lives = 20;
                livesReserve = 2;
                agingDeck.RemoveSpecificAgingCard("AC4"); // very stupid card.
                robinsonDeck.cards.Add(agingDeck.DrawCard());
                break;

            case 3:
                lives = 20;
                livesReserve = 2;
                robinsonDeck.cards.Add(agingDeck.DrawCard());
                break;

            case 4:
                lives = 18;
                livesReserve = 2;
                txtLifeCounter.text = "Lives:" + lives.ToString() + "/" + livesReserve.ToString();
                break;
        }

        // Shuffle again.
        hazardDeck.Shuffle();
        robinsonDeck.Shuffle();
        agingDeck.Shuffle();
    }

    /// <summary>
    /// Amount of lives that robinson has.
    /// </summary>
    /// <returns></returns>
    public int LivesLeft()
    {
        return lives;
    }

    /// <summary>
    /// Get the amount of lives left that are in the reserve.
    /// </summary>
    /// <returns></returns>
    public int LivesReserveLeft()
    {
        return livesReserve;
    }

    private bool isSelecting = false;

    /// <summary>
    /// Enable the GUI mode for the user to select card that he wants to discard.
    /// </summary>
    public void SelectCardsToDiscard()
    {
        // Confirm action and pay. TODO solo se paga cuando se pierde.
        if (isSelecting == false)
        {
            currentGame.canDrawRobCards = false;

            differenceToPayValue = hazardFightingValue - robinsonFightingValue;
            livesToPay = 0;

            isSelecting = true;
            payCardsButtonText.text = "Cancel";

            // Change the call for the card button
            foreach (RobinsonCardScript rc in ge.robinsonDeck.freeRobinsonCards)
            {
                rc.SetOnClickAction(StateOfGame.CHOOSE_CARD);
            }
            foreach (RobinsonCardScript rc in ge.robinsonDeck.freeRobinsonCards)
            {
                rc.SetOnClickAction(StateOfGame.CHOOSE_CARD);
            }
        }
        else
        {
            // Si hay cartas seleccionadas entonces lo hago pagar, sino no
            if (cardsToPayFor.Count > 0)
            {
                if (differenceToPayValue == 0)
                {
                    // Esto es cuando efectivamente paga.
                    this.LooseLife(this.livesToPay);
                    payCardsButtonText.text = "Pay for the rest";
                }
            }
            else
            {
                // Cancel
                currentGame.canDrawRobCards = true;
                differenceToPayValue = -1;
                livesToPay = -1;
            }
        }
    }

    /// <summary>
    /// Add a card to the list of selected cards to pay.
    /// </summary>
    /// <param name="rcs"></param>
    /// <returns></returns>
    public bool AddToRobinsonPayedCard(RobinsonCardScript rcs)
    {
        bool result = false;
        if (currentGame.robinsonFightingValue < currentGame.hazardFightingValue && (differenceToPayValue - rcs._card.lifeCost) >= 0)
        {
            livesToPay += rcs._card.lifeCost;
            differenceToPayValue -= rcs._card.lifeCost;
            this.cardsToPayFor.Add(rcs);
            // Ugly part.
            rcs.gameObject.transform.SetParent(GameObject.Find("Canvas").transform);
            rcs.gameObject.transform.Translate(rcs.gameObject.transform.position.x + 20, rcs.gameObject.transform.position.y, rcs.gameObject.transform.position.z);

            result = true;
        }

        return result;
    }

    /// <summary>
    /// Remove a card to the list of selected cards to pay.
    /// </summary>
    /// <param name="rcs"></param>
    /// <returns></returns>
    public bool RemoveToRobinsonPayedCard(RobinsonCardScript rcs)
    {
        bool result = false;
        if (cardsToPayFor.Contains(rcs) == true)
        {
            livesToPay -= rcs._card.lifeCost;
            differenceToPayValue += rcs._card.lifeCost;

            this.cardsToPayFor.Remove(rcs);

            //rcs.gameObject.transform.SetParent( .transform);
            rcs.gameObject.transform.Translate(rcs.gameObject.transform.position.x + 20, rcs.gameObject.transform.position.y, rcs.gameObject.transform.position.z);
        }

        return result;
    }

    /// <summary>
    /// Gain a specific amount of life from the reserve (if available).
    /// </summary>
    /// <param name="amount"></param>
    public void GainLifeFromReserve(int amount)
    {
        if (livesReserve > 0)
        {
            livesReserve -= amount;
            lives += amount;
            txtLifeCounter.text = "Lives:" + lives.ToString() + "/" + livesReserve.ToString();
        }
    }

    /// <summary>
    /// Rest life amount to Robinson.
    /// </summary>
    /// <param name="amount"></param>
    /// <returns></returns>
    public bool LooseLife(int amount)
    {
        bool result;
        lives = lives - amount;
        livesReserve += amount;

        if (lives < 0)
        {
            // You killed Robinson, you bastard.
            result = true;
        }
        else
        {
            result = false;
        }

        txtLifeCounter.text = "Lives:" + lives.ToString() + "/" + livesReserve.ToString();
        return result;
    }

    /// <summary>
    /// Draw a card from the aging deck.
    /// </summary>
    /// <returns></returns>
    public AgingCard DrawAgingCard()
    {
        return agingDeck.DrawCard();
    }
}