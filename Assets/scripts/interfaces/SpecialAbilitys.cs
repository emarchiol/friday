﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface SpecialAbilitys
{
    // Aging card abilitys only
    void StopAbility();
    void HighestCardAbility();

    // Abilitys.
    void ChangeLifeAbility();
    void DestroyAbility();
    void BelowThePileAbility();
    void PhaseAbility();

    void ExchangeAbility();
    void DoubleExchangeAbility();

    void DrawCardsAbility();
    void CopyAbility();

    void SortCardsAbility();
    void DoubleAbility();
}