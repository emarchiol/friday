﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HazardCardScript : CardScript
{
    public bool canChooseCard = true;
    public HazardDeck _deck;
    public HazardCard _card;
    public RobinsonDeck rbDeck;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    /// <summary>
    /// Set the sprite for the selected card.
    /// </summary>
    /// <param name="card"></param>
    public void SetCard(HazardCard card)
    {
        this._card = card;
        this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(_card.defaultPath + _card.cardID + "_" + _card.robinsonCard.cardID);
    }

    /// <summary>
    /// Method that is called when the user select one of the two hazard card to fight.
    /// </summary>
    public void ChooseCard()
    {
        if (canChooseCard)
        {
            canChooseCard = false;
            _deck.hazardChoose.Remove(this);

            if (_deck.hazardChoose.Count > 0)
            {
                _deck.DiscardUnchoosedCard();
            }
            else
            {
                Destroy(_deck.nextPhaseButton);
                /*
                _deck.nextPhaseButton.gameObject.SetActive(false);
                _deck.nextPhaseButton.transform.SetParent(gameObject.transform);
                */
            }

            _deck.SetFightingCard(this);
            Game.currentGame.ChangeFreeCardsToDraw(this._card.freeCardsValue);
            switch (Game.currentGame.currentAgeDifficult)
            {
                case HazardPhase.GREEN:
                    Game.currentGame.SetHazardFightingValue(_card.greenHazardValue);
                    break;

                case HazardPhase.YELLOW:
                    Game.currentGame.SetHazardFightingValue(_card.yellowHazardValue);
                    break;

                case HazardPhase.RED:
                    Game.currentGame.SetHazardFightingValue(_card.redHazardValue);
                    break;
            }
            // The first free robinson card is mandatory.
            rbDeck.DrawCard();
        }
    }
}