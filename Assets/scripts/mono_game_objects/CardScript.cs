﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardScript : MonoBehaviour
{
    public Sprite backImage;

    /// <summary>
    /// Set the back image for the card.
    /// </summary>
    public void SetBackImage()
    {
        this.gameObject.GetComponent<Image>().sprite = this.backImage;
    }

    /// <summary>
    /// Set the front image for the card.
    /// </summary>
    /// <param name="img"></param>
    public void SetFrontImage(Image img)
    {
        this.gameObject.GetComponent<Image>().sprite = img.GetComponent<Sprite>();
    }
}