﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HazardDeck : Deck
{
    internal bool cardsSpawned = false;

    public RobinsonDeck robinsonDeck;
    public HazardCardScript HazardPrefab;
    public List<HazardCardScript> hazardChoose;
    public List<HazardCard> discardedHazardCards;
    public HazardCardScript hazardFightingCard;

    public Transform hazard_discard_panel;
    public Transform hazard_fighting_panel;

    // Buttons.
    public GameObject nextPhaseButtonPrefab;
    public GameObject nextPhaseButton;

    // Images.
    public Sprite hazardDeckImage;

    Vector3 normalScale = new Vector3(1, 1, 1);

    public static Dictionary<string, int> hazardCopys = new Dictionary<string, int>() {
        { "HC1_RC6", 2 },
        { "HC1_RC10", 2 },
        { "HC1_RC11", 2 },
        { "HC2_RC22", 2 },
        { "HC2_RC26", 2 },
        { "HC4_RC28", 2 }
    };

    /// <summary>
    /// Initialize the internal deck
    /// </summary>
    /// <returns></returns>
    public bool SetUp(List<HazardCard> hazardCards)
    {
        int uniqueID = 1000;
        try
        {
            foreach (HazardCard hcToCopy in hazardCards)
            {
                int repeat = 0;
                string hazardComposedId = string.Empty;
                hazardComposedId = hcToCopy.cardID + "_" + hcToCopy.robinsonCard.cardID;

                if (hazardCopys.TryGetValue(hazardComposedId, out repeat) == false)
                {
                    repeat = 1;
                }

                for (int i = 1; i <= repeat; i++)
                {
                    HazardCard hcCopyCat = new HazardCard(hcToCopy);
                    hcCopyCat.uniqueID = "HC" + uniqueID;
                    uniqueID++;

                    cards.Add(hcCopyCat);
                }
            }

            //Debug.Log("loaded hazard cards into deck: " + cards.Count);
            this.Shuffle();
        }
        catch (Exception e)
        {
            Debug.LogError(string.Format("ERROR - HazardDeck - SetUp - Error while loading cards: " + e.Message, e.StackTrace));
        }

        return true;
    }

    /// <summary>
    /// Shuffle the current deck.
    /// </summary>
    override public void Shuffle()
    {
        int n = this.cards.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            var value = this.cards[k];
            this.cards[k] = this.cards[n];
            this.cards[n] = value;
        }
        // Dar vuelta las cartas.
        foreach (Card c in cards)
        {
            //TODO:
            //c.isFlipped = false;
        }
    }

    /// <summary>
    /// Generate the necessary unity ojects from prefabs to spawn a card from the deck.
    /// </summary>
    public void SpawnCards()
    {
        try
        {
            // TODO: si es una sola hay que agregar un boton en vez de la segunda carta para skipear a la siguiente fase.
            if (this.cards.Count > 0)
            {
                if (cardsSpawned == false)
                {
                    HazardCardScript hcs;
                    hazardChoose = new List<HazardCardScript>();

                    Game.currentGame.canDrawRobCards = true;
                    Game.currentGame.ClearFreeCards();

                    hcs = Instantiate(this.HazardPrefab, this.spawnPanel);
                    hcs.rbDeck = robinsonDeck;
                    hcs.transform.Rotate(new Vector3(0, 0, 180));
                    hcs._deck = this;
                    hcs.SetCard((HazardCard)this.DrawNextCard(true));
                    hazardChoose.Add(hcs);

                    if (cards.Count >= 2)
                    {
                        hcs = Instantiate(this.HazardPrefab, this.spawnPanel);
                        hcs.rbDeck = robinsonDeck;
                        hcs.transform.Rotate(new Vector3(0, 0, 180));
                        hcs._deck = this;
                        hcs.SetCard((HazardCard)this.DrawNextCard(true));
                        hazardChoose.Add(hcs);
                        cardsSpawned = true;
                    }
                    else
                    {
                        nextPhaseButton = Instantiate(this.nextPhaseButtonPrefab, this.spawnPanel);
                        nextPhaseButton.GetComponent<Button>().onClick.AddListener(Game.currentGame.NextDifficultPhase);
                        /*
                        nextPhaseButton.set
                        nextPhaseButton.gameObject.SetA.gameObject.SetActive(true);
                        nextPhaseButton.transform.SetParent(this.spawnPanel);*/
                    }

                    UpdateImageDeck();
                }
            }
        }
        catch (Exception ex)
        {
            Debug.Log("HazardDeck - SpawnCards - error: " + ex.ToString());
        }
    }

    /// <summary>
    /// Get a random card from the deck.
    /// </summary>
    /// <returns></returns>
    override public Card RetrieveRandomCard()
    {
        int n = this.cards.Count - 1;
        int k = rng.Next(n + 1);

        Card c = new HazardCard((HazardCard)this.cards[k]);
        this.cards.RemoveAt(k);

        c = this.cards[k];
        return c;
    }

    /// <summary>
    /// Get all the discarded cards and put them in the hazard deck.
    /// </summary>
    public void RestartDeck()
    {
        DiscardUnchoosedCard();
        Destroy(nextPhaseButton);
        /*
        nextPhaseButton.gameObject.SetActive(false);
        nextPhaseButton.transform.SetParent(gameObject.transform);
        */

        cards.AddRange(discardedHazardCards.Cast<Card>());
        discardedHazardCards.Clear();
        UpdateImageDeck();
    }

    /// <summary>
    /// Change from no image to backcardimage.
    /// </summary>
    public void UpdateImageDeck()
    {
        Color noImage = new Color(1, 1, 1, 0);
        Color fullImage = new Color(1, 1, 1, 1);
        Color partialImage = new Color(1, 1, 1, 0.35f); //90

        if (cards.Count == 0)
        {
            // No image.
            this.gameObject.GetComponent<Image>().sprite = null;
            this.gameObject.GetComponent<Image>().color = noImage;
        }
        else
        {
            // Back image.
            this.gameObject.GetComponent<Image>().sprite = this.hazardDeckImage;
            this.gameObject.GetComponent<Image>().color = fullImage;
        }

        // Discarded deck alpha.
        if (discardedHazardCards.Count == 0)
        {
            hazard_discard_panel.GetComponent<Image>().sprite = null;
            hazard_discard_panel.gameObject.GetComponent<Image>().color = partialImage;
        }
    }

    /// <summary>
    /// TODO: check usage. /*Usar solo el metodo de arriba para esta operacion*/
    /// </summary>
    public void DiscardUnchoosedCard()
    {
        if (hazardChoose.Count > 0)
            DiscardCard(hazardChoose[0]);
        hazardChoose.Clear();

        Game.currentGame.SetPayHazardButtonListener(StateOfGame.CHOOSE_CARD);
    }

    /// <summary>
    /// Set the unity object to fight.
    /// </summary>
    /// <param name="_hcToFight"></param>
    public void SetFightingCard(HazardCardScript _hcToFight)
    {
        _hcToFight.transform.SetParent(hazard_fighting_panel);
        _hcToFight.transform.localScale = normalScale;
        this.hazardFightingCard = _hcToFight;
    }

    /// <summary>
    /// Add a card to the discarded hazard pile.
    /// </summary>
    /// <param name="rcs"></param>
    public void DiscardCard(HazardCardScript hCard)
    {
        try
        {
            discardedHazardCards.Add(hCard._card);
            // no borrar por ahora
            //hCard.transform.SetParent(this.hazard_discard_panel);
            //hCard.transform.localScale = normalScale;

            hazard_discard_panel.GetComponent<Image>().sprite = hCard.GetComponent<Image>().sprite;
            Destroy(hCard.GetComponent<Image>());
            Destroy(hCard.GetComponent<LayoutElement>());
            Destroy(hCard.gameObject);
        }
        catch (Exception ex)
        {
            Debug.Log("AddCardToDiscarded - Adding card to discard pile - error: " + ex.ToString());
        }
    }
}