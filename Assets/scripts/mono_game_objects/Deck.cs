﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Deck : MonoBehaviour {

    public List<Card> cards = new List<Card>();
    public Transform spawnPanel;

	// Use this for initialization
	void Start () {
        //string json = JsonUtility.ToJson(this);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static System.Random rng = new System.Random();

    /// <summary>
    /// Shuffle the current deck.
    /// </summary>
    public abstract void Shuffle();

    /// <summary>
    /// Get one random card from a deck.
    /// </summary>
    /// <param name="deck"></param>
    /// <returns></returns>
    public abstract Card RetrieveRandomCard();

    /// <summary>
    /// Retrieve the next card from bottom or top.
    /// </summary>
    /// <param name="fromTop"></param>
    /// <returns></returns>
    public Card DrawNextCard(bool fromTop = true)
    {
        Card cardToExtract = new Card();
        if (this.cards.Count > 0)
        {
            if (fromTop == true)
            {
                cardToExtract = this.cards[this.cards.Count - 1];
                this.cards.RemoveAt(this.cards.Count - 1);
            }
            else
            {
                cardToExtract = this.cards[0];
                this.cards.RemoveAt(0);
            }
        }
        else
        {
            cardToExtract = null;
        }
        return cardToExtract;
    }

}
