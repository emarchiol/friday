﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class AgingDeck : Deck
{
    public static Dictionary<string, int> agingCopys = new Dictionary<string, int>() {
        { "AC7", 2 },
        { "AC8", 2 }
    };

    /// <summary>
    /// Initialize the internal deck
    /// </summary>
    /// <returns></returns>
    public bool SetUp(List<AgingCard> agingCards)
    {
        int uniqueID = 1000;
        try
        {
            foreach (AgingCard agToCopy in agingCards)
            {
                int repeat = 0;
                if (agingCopys.TryGetValue(agToCopy.cardID, out repeat) == false)
                {
                    repeat = 1;
                }

                for (int i = 1; i <= repeat; i++)
                {
                    AgingCard agCopyCat = new AgingCard(agToCopy);
                    agCopyCat.uniqueID = "AC" + uniqueID;
                    uniqueID++;

                    cards.Add(agCopyCat);
                }
            }
            this.Shuffle();
        }
        catch (Exception e)
        {
            Debug.LogError(string.Format("ERROR - AgingDeck - SetUp - Error while loading cards: " + e.Message, e.StackTrace));
        }

        return true;
    }

    /// <summary>
    /// Shuffle the current deck.
    /// </summary>
    override public void Shuffle()
    {
        int n = this.cards.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            var value = this.cards[k];
            this.cards[k] = this.cards[n];
            this.cards[n] = value;
        }
        // Dar vuelta las cartas.
        foreach (Card c in cards)
        {
            //TODO:
            //c.isFlipped = false;
        }
    }

    /// <summary>
    /// Get a random card from the deck.
    /// </summary>
    /// <returns></returns>
    override public Card RetrieveRandomCard()
    {
        int n = this.cards.Count - 1;
        int k = rng.Next(n + 1);

        Card c = new AgingCard((AgingCard)this.cards[k]);
        this.cards.RemoveAt(k);

        c = this.cards[k];
        return c;
    }

    /// <summary>
    /// Returns an aging card.
    /// </summary>
    public AgingCard DrawCard()
    {
        AgingCard ac = null;
        if (this.cards.Count > 0)
        {
            ac = new AgingCard((AgingCard)cards[0]);
            cards.RemoveAt(0);
        }
        return ac;
    }

    /// <summary>
    /// Remove a card from the deck by id.
    /// </summary>
    /// <param name="cardID"></param>
    /// <returns></returns>
    public bool RemoveSpecificAgingCard(string cardID)
    {
        bool result = false;
        try
        {
            Card ac = cards.Find(a => a.cardID == cardID);
            cards.Remove(ac);
            result = true;
        }
        catch (Exception ex)
        {
            Debug.Log("ERROR - AgingDeck - RemoveSpecificAgingCard - Error while removing aging card: " + cardID + " - " + ex);
        }
        return result;
    }
}