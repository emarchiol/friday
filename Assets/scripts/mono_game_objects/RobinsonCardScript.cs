﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RobinsonCardScript : CardScript, SpecialAbilitys
{
    public RobinsonCard _card;
    public MethodInfo chooseCardMI;

    // Use this for initialization
    void Start()
    {
        // Set the "delegates" for the click button. cuack
        //this.ChooseAsPayedCardMI = this.GetType().GetMethod("ChooseAsPayedCard");
    }

    // Update is called once per frame
    void Update()
    {
    }

    #region abilitys

    // aging call.
    public void StopAbility() { }
    public void HighestCardAbility() { }

    public void DestroyAbility() { }
    public void BelowThePileAbility() { }

    public void PhaseAbility() { }
    public void CopyAbility() { }

    public void SortCardsAbility() { }
    public void DoubleAbility() { }

    public void DoubleExchangeAbility() { }
    public void ExchangeAbility() { }

    // amount required.
    public void DrawCardsAbility() { }
    public void ChangeLifeAbility() { }

    #endregion abilitys

    public void Activate()
    {
        // TODO poner metodos que puedan setear el delegado del click button.
    }

    /// <summary>
    /// Set the correct delegate for clicking the card depending on the given state.
    /// </summary>
    /// <param name="state"></param>
    public void SetOnClickAction(StateOfGame state)
    {
        switch (state)
        {
            case StateOfGame.CHOOSE_CARD:
                this.gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
                this.gameObject.GetComponent<Button>().onClick.AddListener(() => ChooseAsPayedCard());
                break;

            case StateOfGame.SPECIAL_ABILITY:
                this.gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
                this.gameObject.GetComponent<Button>().onClick.AddListener(() => UseAbility());
                break;
        }
    }

    /// <summary>
    /// Use the setted ability.
    /// </summary>
    public void UseAbility()
    {
        // if-else infinito, maybe change this to a switch.
        if (this._card.hasStopAbility)
        {
            StopAbility();
        }
        else if (this._card.hasHighestCardAbility)
        {
            HighestCardAbility();
        }
        else if (this._card.hasDestroyAbility)
        {
            DestroyAbility();
        }
        else if (this._card.hasBelowThePileAbility)
        {
            BelowThePileAbility();
        }
        else if (this._card.hasPhaseAbility)
        {
            PhaseAbility();
        }
        else if (this._card.hasCopyAbility)
        {
            CopyAbility();
        }
        else if (this._card.hasSortCardsAbility)
        {
            SortCardsAbility();
        }
        else if (this._card.hasDoubleAbility)
        {
            DoubleAbility();
        }
        else if (this._card.hasExchangeAbility)
        {
            ExchangeAbility();
        }
        else if (this._card.hasDoubleExchangeAbility)
        {
            DoubleExchangeAbility();
        }
        else if (this._card.hasChangeLifeAbility)
        {
            ChangeLifeAbility();
        }
        else if (this._card.hasDrawCardsAbility)
        {
            DrawCardsAbility();
        }
    }

    /// <summary>
    /// Choose the card as card to be payed with life points. TODO: check this.
    /// </summary>
    private void ChooseAsPayedCard()
    {
        Debug.Log("You have choosen me, i will not fail you...");
    }

    /// <summary>
    /// Set the robinson card as 
    /// </summary>
    /// <param name="card"></param>
    public void SetCard(RobinsonCard card)
    {
        this._card = card;
        Debug.Log("loading from: " + _card.defaultPath);
        this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(_card.defaultPath + _card.cardID);
        Game.currentGame.ChangeRobinsonFightingValue(_card.fightingValue);
    }
}