﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RobinsonDeck : Deck
{
    public Transform spawnPayedCardsPanel;
    public Transform discardedCardsPanel;
    public RobinsonCardScript robinsonPrefab;

    // Images.
    public Sprite robinsonDeckImage;
    public Sprite discardedDeckLastCardImage;

    // lists of cards in around the game.
    public List<RobinsonCardScript> freeRobinsonCards;
    public List<RobinsonCardScript> payedRobinsonCards;
    public List<RobinsonCard> discardedRobinsonCards;

    public static Dictionary<string, int> robinsonCopys = new Dictionary<string, int>() {
        { "RC1", 1 },
        { "RC2", 8 },
        { "RC3", 3 },
        { "RC4", 1 },
        { "RC5", 5 }
    };

    /// <summary>
    /// Initialize the internal deck
    /// </summary>
    /// <returns></returns>
    public bool SetUp(List<RobinsonCard> robinsonCards, AgingCard agingCardToShuffle)
    {
        int uniqueID = 1000;
        try
        {
            foreach (string cardIDToCopy in robinsonCopys.Keys)
            {
                // TODO tomá la carta a duplicar y duplicala tantas veces como diga el dictionary.
                if (robinsonCards.Where(c => c.cardID == cardIDToCopy).Count() > 0)
                {
                    RobinsonCard rcToCopy = robinsonCards.Find(c => c.cardID == cardIDToCopy);

                    int amountToSpawn = robinsonCopys[cardIDToCopy];
                    for (int i = 0; i < amountToSpawn; i++)
                    {
                        RobinsonCard rcCopyCat = new RobinsonCard(rcToCopy);
                        rcCopyCat.uniqueID = "RC" + uniqueID;
                        uniqueID++;
                        cards.Add(rcCopyCat);
                    }
                }
            }
            //Debug.Log("loaded robinson cards into deck: " + cards.Count);
            this.Shuffle();

            this.freeRobinsonCards = new List<RobinsonCardScript>();
            this.payedRobinsonCards = new List<RobinsonCardScript>();
        }
        catch (Exception e)
        {
            Debug.LogError(string.Format("ERROR - RobinsonDeck - SetUp - Error while loading cards: " + e.Message, e.StackTrace));
        }

        return true;
    }

    /// <summary>
    /// Draw a card from the robinson deck and display it as a free or payed card.
    /// </summary>
    public void DrawCard()
    {
        // La fase de pelea está determinada por el hazardFightingValue, si es -1 entonces está en otra fase, (esto es unicamente para que no saque cartas)
        // la segunda condición es que no puede seguir sacando cartas si ya ganó contra la hazard, aunque esto no implica que haya terminado la fase de pelea.
        Debug.Log("trying to draw free card:" + Game.currentGame.hazardFightingValue + "/" + Game.currentGame.robinsonFightingValue + "/" + Game.currentGame.canDrawRobCards);
        if ((Game.currentGame.hazardFightingValue >= 0) && ((Game.currentGame.robinsonFightingValue < Game.currentGame.hazardFightingValue) || Game.currentGame.canDrawRobCards == true))
        {
            if (this.cards.Count > 0)
            {
                if (Game.currentGame.FreeCardsToDraw() > 0)
                {
                    // Yeeey free stuff.
                    Game.currentGame.ChangeFreeCardsToDraw(-1);
                    freeRobinsonCards.Add(SpawnCard(this.spawnPanel));
                }
                else if (Game.currentGame.LivesLeft() > 0)
                {
                    if (Game.currentGame.robinsonFightingValue < Game.currentGame.hazardFightingValue)
                    {
                        Game.currentGame.LooseLife(1);
                        payedRobinsonCards.Add(SpawnCard(this.spawnPayedCardsPanel));
                    }
                }
            }
            else
            {
                // Si el mazo está vacío se re construye con las discardeds +1 carta aging.
                RestartDeck();
            }
        }
    }

    /// <summary>
    /// Add a RobinsonCardScript to the discarded robinson deck.
    /// </summary>
    /// <param name="rcs"></param>
    public void AddCardToDiscarded(RobinsonCardScript rcs)
    {
        try
        {
            discardedRobinsonCards.Add(rcs._card);
            discardedCardsPanel.GetComponent<Image>().sprite = rcs.GetComponent<Image>().sprite;
            Destroy(rcs.GetComponent<Image>());
            Destroy(rcs.GetComponent<LayoutElement>());
            Destroy(rcs.gameObject);
        }
        catch (Exception ex)
        {
            Debug.Log("AddCardToDiscarded - Adding card to discard pile - error: " + ex.ToString());
        }
    }

    /// <summary>
    /// Add a robinsonCard to the discardpanel, this will load the image from the assets.
    /// </summary>
    /// <param name="rcs"></param>
    public void AddCardToDiscarded(HazardCard hc)
    {
        try
        {
            RobinsonCard rc = new RobinsonCard(hc.robinsonCard);
            rc.cardID = hc.cardID + "_" + rc.cardID;
            rc.defaultPath = hc.defaultPath;
            rc.uniqueID = hc.uniqueID;

            discardedRobinsonCards.Add(rc);
            //string defaultRCPath = "front_cards/hc/";
            discardedCardsPanel.GetComponent<Image>().sprite = Resources.Load<Sprite>(rc.defaultPath + rc.cardID);
        }
        catch (Exception ex)
        {
            Debug.Log("AddCardToDiscarded - Adding card to discard pile - error: " + ex.ToString());
        }
    }

    /// <summary>
    /// Draw and spawn a card from the robinsondeck
    /// </summary>
    /// <param name="panelToSpawn"></param>
    /// <returns></returns>
    public RobinsonCardScript SpawnCard(Transform panelToSpawn)
    {
        RobinsonCardScript rcs = null;
        try
        {
            if (this.cards.Count > 0)
            {
                rcs = Instantiate(this.robinsonPrefab, panelToSpawn);
                rcs.SetCard((RobinsonCard)this.DrawNextCard(true));
            }
        }
        catch (Exception ex)
        {
            Debug.Log("RobinsonDeck - SpawnCard - error: " + ex.ToString());
        }

        UpdateImageDeck();
        return rcs;
    }

    /// <summary>
    /// Shuffle the current deck.
    /// </summary>
    override public void Shuffle()
    {
        int n = this.cards.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            var value = this.cards[k];
            this.cards[k] = this.cards[n];
            this.cards[n] = value;
        }
        // Dar vuelta las cartas.
        foreach (Card c in cards)
        {
            //TODO:
            //c.isFlipped = false;
        }
    }

    /// <summary>
    /// Get a random card from the deck.
    /// </summary>
    /// <returns></returns>
    override public Card RetrieveRandomCard()
    {
        int n = this.cards.Count - 1;
        int k = rng.Next(n + 1);

        Card c = new RobinsonCard((RobinsonCard)this.cards[k]);
        this.cards.RemoveAt(k);

        c = this.cards[k];
        return c;
    }

    /// <summary>
    /// Get all the discarded cards and put them in the hazard deck + add an aging card if available.
    /// </summary>
    public void RestartDeck()
    {
        cards.AddRange(discardedRobinsonCards.Cast<Card>());
        // Add the aging card.
        AgingCard ac = Game.currentGame.DrawAgingCard();
        cards.Add(ac);

        discardedRobinsonCards.Clear();
        UpdateImageDeck();
    }

    /// <summary>
    /// Change from no image to backcardimage.
    /// </summary>
    public void UpdateImageDeck()
    {
        Color noImage = new Color(1, 1, 1, 0);
        Color fullImage = new Color(1, 1, 1, 1);
        Color partialImage = new Color(1, 1, 1, 0.35f); //90

        if (cards.Count == 0)
        {
            // No image.
            this.gameObject.GetComponent<Image>().sprite = null;
            this.gameObject.GetComponent<Image>().color = noImage;
        }
        else
        {
            // Back image.
            this.gameObject.GetComponent<Image>().sprite = this.robinsonDeckImage;
            this.gameObject.GetComponent<Image>().color = fullImage;
        }

        // Discarded deck alpha.
        if (discardedRobinsonCards.Count == 0)
        {
            discardedCardsPanel.GetComponent<Image>().sprite = null;
            discardedCardsPanel.gameObject.GetComponent<Image>().color = partialImage;
        }
    }
}